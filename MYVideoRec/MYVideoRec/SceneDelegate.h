//
//  SceneDelegate.h
//  MYVideoRec
//
//  Created by meng yun on 2020/9/26.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

