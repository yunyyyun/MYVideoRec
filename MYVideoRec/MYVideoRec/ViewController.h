//
//  ViewController.h
//  MYVideoRec
//
//  Created by meng yun on 2020/9/26.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@end

@interface ZoomIndicatorView : UIView

- (void)setProgress:(CGFloat)p;

@end
