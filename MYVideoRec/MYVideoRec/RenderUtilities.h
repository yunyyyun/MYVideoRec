//
//  RenderUtilities.h
//  MYVideoRecorder
//
//  Created by 孟云 on 20/9/14.
//  Copyright © 2020年 孟云. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>

@interface RenderUtilities : NSObject

- (CVPixelBufferRef)progressPixelBuffer:(CVPixelBufferRef)pixelBuffer;

@end
