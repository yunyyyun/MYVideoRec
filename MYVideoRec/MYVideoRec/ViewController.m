//
//  ViewController.m
//  MYVideoRec
//
//  Created by meng yun on 2020/9/26.
//

#import "ViewController.h"
#import "MYVideoRecorder.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "MYVideoRecorderUtilities.h"
#import "RenderUtilities.h"

@interface ViewController ()<MYVideoRecorderDelegate>{
    MYVideoRecorderPreview *preview;
    UIView *swithView;
    UIButton *capturePhotoButton;
    UIButton *recordButton;
    UIButton *flipButton;
    UIButton *torchButton;
    
    UIButton *effectsChangeButton;
    
    UIImageView *focusImageView;
    ZoomIndicatorView *zoomIndicatorView;
    UILabel *timeLabel;
    NSTimer *timer;//录制进行中
    CGFloat currentTime;//当前已录制时间
    CGFloat preZoomFactor;
    RenderUtilities *renderUtilities;
    
    NSArray *shaderEffectsDescs;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    preZoomFactor = 1;
    renderUtilities = [[RenderUtilities alloc] init];
    
    [self initMYVideoRecorder];
    [self initViews];
    
    shaderEffectsDescs = @[@"正常", @"灰", @"红", @"绿", @"蓝", @"马赛克", @"反"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[MYVideoRecorder sharedInstance] startPreview];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[MYVideoRecorder sharedInstance] stopPreview];
}

- (void)initViews
{
    self.view.backgroundColor = [UIColor blackColor];
    
    preview = [[MYVideoRecorder sharedInstance] preview];
    preview.shaderEffectsType = 0;
    preview.frame = self.view.bounds;
    [self.view addSubview:preview];
    
    focusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"FocusCamera"]];
    focusImageView.alpha = 0.0f;
    [preview addSubview:focusImageView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(focusAction:)];
    [preview addGestureRecognizer:tapGesture];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoomAction:)];
    [preview addGestureRecognizer:pinchGesture];
    
    swithView = [[UIView alloc] initWithFrame:preview.frame];
    swithView.backgroundColor = [UIColor blackColor];
    swithView.hidden = YES;
    [preview addSubview:swithView];
    
    CGFloat topNavHeight = 30;
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 44+topNavHeight)];
    topView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    [self.view addSubview:topView];
    
    flipButton = [UIButton buttonWithType:UIButtonTypeCustom];
    flipButton.frame = CGRectMake(CGRectGetWidth(topView.frame)-44, topNavHeight, 44, 44);
    [flipButton setImage:[UIImage imageNamed:@"FilpCamera"] forState:UIControlStateNormal];
    [flipButton addTarget:self action:@selector(flipCamera:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:flipButton];
    
    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(44, topNavHeight, topView.frame.size.width-44*2, 44)];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.font = [UIFont systemFontOfSize:17];
    timeLabel.text = @"00:00:00";
    [topView addSubview:timeLabel];
    
    torchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    torchButton.frame = CGRectMake(0, topNavHeight, 44, 44);
    [torchButton setImage:[UIImage imageNamed:@"TorchClose"] forState:UIControlStateNormal];
    [torchButton addTarget:self action:@selector(switchTorch:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:torchButton];
    
    CGFloat bottomTabHeight = 10;
    CGFloat bottomViewHeight = bottomTabHeight + 100;
    CGFloat bottomViewWidth = self.view.frame.size.width;
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                  self.view.frame.size.height - bottomViewHeight,
                                                                  bottomViewWidth,
                                                                  bottomViewHeight)];
    bottomView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    [self.view addSubview:bottomView];
    
    zoomIndicatorView = [[ZoomIndicatorView alloc] initWithFrame:CGRectMake(10,
                                                                            self.view.frame.size.height-bottomViewHeight-8,
                                                                            CGRectGetWidth(self.view.frame)-10*2,
                                                                            5)];
    zoomIndicatorView.alpha = 0.0f;
    [self.view addSubview:zoomIndicatorView];
    
    recordButton = [UIButton buttonWithType:UIButtonTypeCustom];
    recordButton.frame = CGRectMake(bottomViewWidth/2.0f-42, 8, 84, 84);
    [recordButton setImage:[UIImage imageNamed:@"StartRecorder"] forState:UIControlStateNormal];
    [recordButton setImage:[UIImage imageNamed:@"StopRecorder"] forState:UIControlStateSelected];
    [recordButton setImage:[UIImage imageNamed:@"CantRecorder"] forState:UIControlStateDisabled];
    [recordButton addTarget:self action:@selector(recordAction:) forControlEvents:UIControlEventTouchUpInside];
    recordButton.enabled = NO;
    [bottomView addSubview:recordButton];
    
    capturePhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    capturePhotoButton.frame = CGRectMake((CGRectGetWidth(bottomView.frame)/2.0f-83/2.0f)/2.0f-44/2.0f, bottomView.frame.size.height/2.0f-44/2.0f, 44, 44);
    [capturePhotoButton setTitle:@"截图" forState:UIControlStateNormal];
    [capturePhotoButton addTarget:self action:@selector(captureFrameAsPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:capturePhotoButton];
    
    effectsChangeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [effectsChangeButton setTitleColor: [UIColor systemRedColor] forState: UIControlStateNormal];
    effectsChangeButton.frame = CGRectMake(bottomViewWidth-88, 0, 88, bottomViewHeight);
    [effectsChangeButton setTitle:@"正常" forState:UIControlStateNormal];
    [effectsChangeButton addTarget:self action:@selector(effectsChange:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:effectsChangeButton];
}

- (void)initMYVideoRecorder
{
    MYVideoRecorder *videoRecorder = [MYVideoRecorder sharedInstance];
    videoRecorder.delegate = self;
    videoRecorder.cameraDevice = XMCameraDeviceFront;
    videoRecorder.cameraOrientation = XMCameraOrientationPortrait;
    videoRecorder.outputFormat = XMOutputFormatPreset;
    videoRecorder.captureDirectory = NSTemporaryDirectory();
    videoRecorder.captureSessionPreset = AVCaptureSessionPreset640x480;
    videoRecorder.videoBitRate = XMVideoBitRate640x480;
    videoRecorder.additionalCompressionProperties = @{AVVideoProfileLevelKey : AVVideoProfileLevelH264Baseline30};
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - timer

- (void)initTimer
{
    if(timer==nil){
        timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTimeLabel) userInfo:nil repeats:YES];
    }
}

- (void)unInitTimer
{
    if(timer!=nil){
        [timer invalidate];
        timer = nil;
    }
}

- (void)updateTimeLabel
{
    currentTime += 0.1;
    timeLabel.text = [MYVideoRecorderUtilities GetVideoLengthStringFromSeconds:currentTime];
}

#pragma mark - action

- (void)switchTorch:(UIButton *)sender
{
    MYVideoRecorder *recorder = [MYVideoRecorder sharedInstance];
    XMTorchMode newTorchMode = XMTorchModeOff;
    switch (recorder.torchMode) {
        case XMTorchModeOff:
            newTorchMode = XMTorchModeAuto;
            [torchButton setImage:[UIImage imageNamed:@"TorchAuto"] forState:UIControlStateNormal];
            break;
        case XMTorchModeAuto:
            newTorchMode = XMTorchModeOn;
            [torchButton setImage:[UIImage imageNamed:@"TorchOpen"] forState:UIControlStateNormal];
            break;
        case XMTorchModeOn:
            newTorchMode = XMTorchModeOff;
            [torchButton setImage:[UIImage imageNamed:@"TorchClose"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    recorder.torchMode = newTorchMode;
}

- (void)flipCamera:(UIButton *)sender
{
    MYVideoRecorder *recorder = [MYVideoRecorder sharedInstance];
    recorder.cameraDevice = recorder.cameraDevice == XMCameraDeviceBack ? XMCameraDeviceFront : XMCameraDeviceBack;
}

- (void)zoomAction:(UIPinchGestureRecognizer *)pinchGesture
{
    CGFloat zoomFactor = preZoomFactor * pinchGesture.scale;
    if (zoomFactor > [MYVideoRecorder sharedInstance].maxZoomFactor) {
        zoomFactor = [MYVideoRecorder sharedInstance].maxZoomFactor;
    }else if (zoomFactor < [MYVideoRecorder sharedInstance].minZoomFactor){
        zoomFactor = [MYVideoRecorder sharedInstance].minZoomFactor;
    }
    
    if (zoomIndicatorView.alpha == 0) {
        zoomIndicatorView.alpha = 1.0f;
    }
    [zoomIndicatorView setProgress:(zoomFactor-1)/([MYVideoRecorder sharedInstance].maxZoomFactor-1)];
    
    [[MYVideoRecorder sharedInstance] setVideoZoomFactor:zoomFactor withRate:pinchGesture.velocity*2];
    
    if ([pinchGesture state] == UIGestureRecognizerStateEnded ||
        [pinchGesture state] == UIGestureRecognizerStateCancelled ||
        [pinchGesture state] == UIGestureRecognizerStateFailed) {
        preZoomFactor = zoomFactor;
        
        [UIView animateWithDuration:1.5 animations:^{
            self->zoomIndicatorView.alpha = 0.0f;
        }];
    }
}

- (void)focusAction:(UITapGestureRecognizer *)tapGesture
{
    CGPoint tapPoint = [tapGesture locationInView:preview];
    
    CGRect focusFrame = focusImageView.frame;
    focusFrame.origin.x = tapPoint.x - (focusFrame.size.width * 0.5);
    focusFrame.origin.y = tapPoint.y - (focusFrame.size.height * 0.5);
    [focusImageView setFrame:focusFrame];
    
    focusImageView.transform = CGAffineTransformMakeScale(1.8f, 1.8f);
    focusImageView.alpha = 0.0f;
    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self->focusImageView.transform = CGAffineTransformIdentity;
        self->focusImageView.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self->focusImageView.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
        } completion:^(BOOL finished1) {
            self->focusImageView.alpha = 0.0f;
        }];
    }];
    
    [[MYVideoRecorder sharedInstance] focusExposeAndAdjustWhiteBalanceAtAdjustedPoint:tapPoint];
}

- (void)captureFrameAsPhoto:(UIButton *)sender
{
    [[MYVideoRecorder sharedInstance] captureVideoFrameAsPhoto];
}

- (void)effectsChange:(UIButton *)sender
{
    preview.shaderEffectsType = (preview.shaderEffectsType+1) % shaderEffectsDescs.count;
    
    int index = preview.shaderEffectsType;
    NSString *s = shaderEffectsDescs[index];
    [effectsChangeButton setTitle: s forState:UIControlStateNormal];
}

- (void)recordAction:(UIButton *)sender
{
    if (sender.selected) {
        recordButton.enabled = NO;
        [[MYVideoRecorder sharedInstance] endVideoCapture];
    }else{
        flipButton.hidden = YES;
        torchButton.hidden = YES;
        [self initTimer];
        [[MYVideoRecorder sharedInstance] startVideoCapture];
    }
    sender.selected = !sender.selected;
}

#pragma mark - alert
- (void)showAlertViewWithMessage:(NSString *)message title:(NSString *)title
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle: title
                                                                    message: message
                                                             preferredStyle: UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle: @"确定"
                                                       style: UIAlertActionStyleDefault
                                                     handler: nil];
    [alert addAction:okAction];
    
    [self presentViewController: alert animated: YES completion: nil];
    currentTime = 0.0f;
    timeLabel.text = @"00:00:00";
    recordButton.enabled = YES;
    flipButton.hidden = NO;
    torchButton.hidden = ![MYVideoRecorder sharedInstance].isTorchAvailable;
}

#pragma mark - MYVideoRecorderDelegate
//实时处理视频数据，把处理后的数据传回MYVideoRecorder
- (CVPixelBufferRef)recorderWillRenderAndWritePixelBuffer:(CVPixelBufferRef)orginPixelBuffer
{
    return [renderUtilities progressPixelBuffer:orginPixelBuffer];
}

- (void)recorderSessionDidStart:(MYVideoRecorder *)recorder
{
    recordButton.enabled = YES;
    flipButton.hidden = NO;
    torchButton.hidden = !recorder.isTorchAvailable;
}

- (void)recorderSessionDidStop:(MYVideoRecorder *)recorder
{
    recordButton.enabled = NO;
    flipButton.hidden = YES;
    torchButton.hidden = YES;
}

- (void)recorderCameraDeviceWillChange:(MYVideoRecorder *)recorder
{
    swithView.hidden = NO;
    recordButton.enabled = NO;
    capturePhotoButton.enabled = NO;
    torchButton.hidden = YES;
}

- (void)recorderCameraDeviceDidChange:(MYVideoRecorder *)recorder
{
    swithView.hidden = YES;
    recordButton.enabled = YES;
    capturePhotoButton.enabled = YES;
    torchButton.hidden = !recorder.isTorchAvailable;
}

- (NSString *)recorder:(MYVideoRecorder *)recorder willStartVideoCaptureToFile:(NSString *)fileName
{
    NSTimeInterval currentTimeInterval = [[NSDate date] timeIntervalSince1970];
    NSString *localVideoName=[NSString stringWithFormat:@"XMVideo_%.0f.mp4",currentTimeInterval*1000];
    
    return localVideoName;
}

- (void)recorder:(MYVideoRecorder *)recorder capturedVideo:(NSDictionary *)videoDict error:(NSError *)error
{
    [self unInitTimer];
    
    NSString *videoPath = [videoDict objectForKey:MYVideoRecorderVideoPathKey];
    CGFloat videoDuration = [[videoDict objectForKey:MYVideoRecorderVideoCapturedDurationKey] floatValue];
    NSLog(@"视频路径：%@，视频时长：%f",videoPath,videoDuration);
    
    if (error && [error.domain isEqual:MYVideoRecorderErrorDomain] && error.code == MYVideoRecorderErrorCancelled) {
        NSLog(@"recording session cancelled");
        return;
    } else if (error) {
        NSLog(@"encounted an error in video capture (%@)", error);
        return;
    }
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    __weak typeof(self) weakSelf = self;
    [library writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:videoPath]
                                completionBlock:^(NSURL *assetURL, NSError *error){
                                    if (error) {
                                        NSString *mssg = [NSString stringWithFormat:@"Error saving the video to the photo library. %@", error];
                                        [weakSelf showAlertViewWithMessage:mssg title:nil];
                                    }
                                    else {
                                        [weakSelf showAlertViewWithMessage:@"视频保存到相册了" title:@"告诉你一声"];
                                    }
                                }];
}

- (void)recorderWillCapturePhoto:(MYVideoRecorder *)recorder
{
    capturePhotoButton.enabled = NO;
}

- (void)recorderDidCapturePhoto:(MYVideoRecorder *)recorder
{
    capturePhotoButton.enabled = YES;
}

- (void)recorder:(MYVideoRecorder *)recorder capturedPhoto:(nullable NSDictionary *)photoDict error:(nullable NSError *)error
{
    UIImage *image = [photoDict objectForKey:MYVideoRecorderPhotoImageKey];
    
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        NSString *mssg = [NSString stringWithFormat:@"Error saving the photo to the photo library. %@", error];
        [self showAlertViewWithMessage:mssg title:nil];
    }
    else {
        [self showAlertViewWithMessage:@"截图保存到相册了" title:@"告你一声"];
    }

}

@end


@interface ZoomIndicatorView()
{
    CGFloat progress;
}
@end

@implementation ZoomIndicatorView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 5/2.0f;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context        = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    {
        CGContextClearRect(context, rect);
        
        CGContextSetRGBFillColor(context, 255/255.0f, 255/255.0f, 255/255.0f, 1.0);
        CGContextFillRect(context, rect);
        
        CGContextSetRGBFillColor(context, 255/255.0f, 200/255.0f, 0/255.0f, 1.0);
        rect.size.width = progress*self.bounds.size.width;
        
        CGContextFillRect(context, rect);
        CGContextStrokePath(context);
        
    }
    CGContextRestoreGState(context);
}

- (void)setProgress:(CGFloat)p
{
    progress = p;
    [self setNeedsDisplay];
}

@end

