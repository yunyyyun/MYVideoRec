//
//  MYVideoRecorderPreview.h
//  MYVideoRecorder
//
//  Created by 孟云 on 20/9/29.
//  Copyright © 2020年 孟云. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYVideoRecorderPreview : UIView

@property (nonatomic, assign) int shaderEffectsType;  // 特效类型

- (void)displayPixelBuffer:(CVPixelBufferRef)pixelBuffer;
- (void)flushPixelBufferCache;
- (void)reset;

@end
