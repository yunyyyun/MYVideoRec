//
//  Shader.fsh
//  ES2
//
//  Created by mengyun on 16/2/2.
//  Copyright © 2016年 mengyun. All rights reserved.
//
// varying highp vec4 v_fragmentColor;

#ifdef GL_ES
    precision mediump float;
#endif

varying highp vec2 coordinate;
uniform sampler2D videoframe;
uniform highp int shaderEffectsType;

void main()
{
    gl_FragColor = texture2D(videoframe, coordinate);

    // 特效
    if (shaderEffectsType == 0 ) {

    } else if (shaderEffectsType == 1 ) { // 灰
        // highp double v = (gl_FragColor.r+gl_FragColor.g+gl_FragColor.b)/3.0;
        gl_FragColor.r = (gl_FragColor.r+gl_FragColor.g+gl_FragColor.b)/3.0;
        gl_FragColor.b = (gl_FragColor.r+gl_FragColor.g+gl_FragColor.b)/3.0;
        gl_FragColor.g = (gl_FragColor.r+gl_FragColor.g+gl_FragColor.b)/3.0;

    } else if (shaderEffectsType == 2 ) { // 红
        
        gl_FragColor.r = 0.8;
        
    } else if (shaderEffectsType == 3 ) { // 绿
        
        gl_FragColor.g = 0.8;
        
    } else if (shaderEffectsType == 4 ) { // 蓝
        
        gl_FragColor.b = 0.8;
        
    } else if (shaderEffectsType == 5 ) { // 马赛克
        
        if (coordinate.y < 0.5) {
            return;
        }
        
        float dd = 128.0;
        float cx = float(int(coordinate.x*dd))/dd;
        float cy = float(int(coordinate.y*dd))/dd;
        
        
        vec4 c1 = gl_FragColor = texture2D(videoframe, vec2(cx, cy));
        gl_FragColor = c1;
        
    } else if (shaderEffectsType == 6 ) { // 反
       
        gl_FragColor.r = 1.0 - gl_FragColor.r;
        gl_FragColor.g = 1.0 - gl_FragColor.g;
        gl_FragColor.b = 1.0 - gl_FragColor.b;
       
   }
}
