//
//  Shader.vsh
//  ES2
//
//  Created by mengyun on 16/2/2.
//  Copyright © 2016年 mengyun. All rights reserved.
//

attribute vec4 position;
attribute mediump vec4 texturecoordinate;
varying mediump vec2 coordinate;
void main()
{
    gl_Position = position;
    coordinate = texturecoordinate.xy;
}
