//
//  MYVideoRecorder.h
//  MYVideoRecorder
//
//  Created by 孟云 on 20/9/28.
//  Copyright © 2020年 孟云. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "MYVideoRecorderPreview.h"

typedef NS_ENUM(NSInteger, XMCameraDevice) {
    XMCameraDeviceBack = 0,
    XMCameraDeviceFront
};

typedef NS_ENUM(NSInteger, XMCameraOrientation) {
    XMCameraOrientationPortrait = AVCaptureVideoOrientationPortrait,
    XMCameraOrientationPortraitUpsideDown = AVCaptureVideoOrientationPortraitUpsideDown,
    XMCameraOrientationLandscapeRight = AVCaptureVideoOrientationLandscapeRight,
    XMCameraOrientationLandscapeLeft = AVCaptureVideoOrientationLandscapeLeft,
};

typedef NS_ENUM(NSInteger, XMFocusMode) {
    XMFocusModeLocked = AVCaptureFocusModeLocked,
    XMFocusModeAutoFocus = AVCaptureFocusModeAutoFocus,
    XMFocusModeContinuousAutoFocus = AVCaptureFocusModeContinuousAutoFocus
};

typedef NS_ENUM(NSInteger, XMExposureMode) {
    XMExposureModeLocked = AVCaptureExposureModeLocked,
    XMExposureModeAutoExpose = AVCaptureExposureModeAutoExpose,
    XMExposureModeContinuousAutoExposure = AVCaptureExposureModeContinuousAutoExposure
};

typedef NS_ENUM(NSInteger, XMTorchMode) {
    XMTorchModeOff = AVCaptureTorchModeOff,
    XMTorchModeOn = AVCaptureTorchModeOn,
    XMTorchModeAuto = AVCaptureTorchModeAuto
};

typedef NS_ENUM(NSInteger, XMMirroringMode) {
    XMMirroringAuto = 0,
    XMMirroringOn,
    XMMirroringOff
};

typedef NS_ENUM(NSInteger, XMOutputFormat) {
    XMOutputFormatPreset = 0,
    XMOutputFormatSquare, // 1:1
    XMOutputFormatWidescreen, // 16:9
    XMOutputFormatStandard // 4:3
};

NS_ASSUME_NONNULL_BEGIN

// XMError

extern NSString * const MYVideoRecorderErrorDomain;

typedef NS_ENUM(NSInteger, MYVideoRecorderErrorType)
{
    MYVideoRecorderErrorUnknown = -1,
    MYVideoRecorderErrorCancelled = 100,
    MYVideoRecorderErrorSessionFailed = 101,
    MYVideoRecorderErrorBadOutputFile = 102,
    MYVideoRecorderErrorOutputFileExists = 103,
    MYVideoRecorderErrorCaptureFailed = 104,
};

// additional video capture keys
extern NSString * const MYVideoRecorderVideoRotation;
// photo dictionary keys
extern NSString * const MYVideoRecorderPhotoMetadataKey;
extern NSString * const MYVideoRecorderPhotoJPEGKey;
extern NSString * const MYVideoRecorderPhotoImageKey;
extern NSString * const MYVideoRecorderPhotoThumbnailKey; // 160x120

// video dictionary keys
extern NSString * const MYVideoRecorderVideoPathKey;
extern NSString * const MYVideoRecorderVideoThumbnailKey;
extern NSString * const MYVideoRecorderVideoThumbnailArrayKey;
extern NSString * const MYVideoRecorderVideoCapturedDurationKey; // Captured duration in seconds

// suggested videoBitRate constants
static CGFloat const XMVideoBitRate480x360 = 87500 * 8;
static CGFloat const XMVideoBitRate640x480 = 437500 * 8;
static CGFloat const XMVideoBitRate1280x720 = 1312500 * 8;
static CGFloat const XMVideoBitRate1920x1080 = 2975000 * 8;
static CGFloat const XMVideoBitRate960x540 = 3750000 * 8;
static CGFloat const XMVideoBitRate1280x750 = 5000000 * 8;



@protocol MYVideoRecorderDelegate;
@interface MYVideoRecorder : NSObject

@property (nonatomic, weak, nullable) id<MYVideoRecorderDelegate> delegate;
// session
@property (nonatomic, readonly, getter=isCaptureSessionActive) BOOL captureSessionActive;
// setup
@property (nonatomic) XMCameraOrientation cameraOrientation;
@property (nonatomic) XMCameraDevice cameraDevice;
//使用自己配置的AVAudioSession，停止AVCaptureSession对AVAudioSession的自动设置，比如支持蓝牙耳机的录制就要设置成YES。
@property (nonatomic) BOOL usesApplicationAudioSession;
@property (nonatomic) XMTorchMode torchMode; // torch
@property (nonatomic, readonly, getter=isTorchAvailable) BOOL torchAvailable;
@property (nonatomic) XMMirroringMode mirroringMode;
// video output settings
@property (nonatomic, copy) NSDictionary *additionalVideoProperties;
@property (nonatomic, copy) NSString *captureSessionPreset;
@property (nonatomic, copy) NSString *captureDirectory;
@property (nonatomic) XMOutputFormat outputFormat;
// video compression settings
@property (nonatomic) CGFloat videoBitRate;
@property (nonatomic) NSDictionary *additionalCompressionProperties;
// video frame rate (adjustment may change the capture format (AVCaptureDeviceFormat : FoV, zoom factor, etc)
@property (nonatomic) NSInteger videoFrameRate; // desired fps for active cameraDevice
// preview
@property (nonatomic, readonly) MYVideoRecorderPreview *preview;
@property (nonatomic, readonly) CGRect cleanAperture;
// focus, exposure
@property (nonatomic) XMFocusMode focusMode;
@property (nonatomic) XMExposureMode exposureMode;
//zoomFactor
@property (nonatomic, readonly) NSUInteger maxZoomFactor;
@property (nonatomic, readonly) NSUInteger minZoomFactor;
// video
@property (nonatomic, readonly) BOOL supportsVideoCapture;
@property (nonatomic, readonly) BOOL canCaptureVideo;
@property (nonatomic, readonly, getter=isRecording) BOOL recording;
@property (nonatomic, readonly, getter=isPaused) BOOL paused;
@property (nonatomic, getter=isAudioCaptureEnabled) BOOL audioCaptureEnabled;
@property (nonatomic, readonly) EAGLContext *context;
@property (nonatomic) CMTime maximumCaptureDuration;
@property (nonatomic, readonly) Float64 capturedAudioSeconds;
@property (nonatomic, readonly) Float64 capturedVideoSeconds;
// thumbnails
@property (nonatomic) BOOL thumbnailEnabled; // thumbnail generation, disabling reduces processing time for a photo or video


+ (MYVideoRecorder *)sharedInstance;

- (BOOL)supportsVideoFrameRate:(NSInteger)videoFrameRate;
- (void)startPreview;
- (void)stopPreview;
// note: focus and exposure modes change when adjusting on point
- (void)adjustFocusExposureAndWhiteBalance;
- (BOOL)isFocusPointOfInterestSupported;
- (void)focusExposeAndAdjustWhiteBalanceAtAdjustedPoint:(CGPoint)adjustedPoint;
- (void)focusAtAdjustedPointOfInterest:(CGPoint)adjustedPoint;
- (BOOL)isAdjustingFocus;
- (void)exposeAtAdjustedPointOfInterest:(CGPoint)adjustedPoint;
- (BOOL)isAdjustingExposure;
- (void)setVideoZoomFactor:(CGFloat)factor withRate:(float)rate;
// photo
- (void)captureVideoFrameAsPhoto;
// video
- (void)startVideoCapture;
- (void)pauseVideoCapture;
- (void)resumeVideoCapture;
- (void)endVideoCapture;
- (void)cancelVideoCapture;
// thumbnails
- (void)captureCurrentVideoThumbnail;
- (void)captureVideoThumbnailAtFrame:(int64_t)frame;
- (void)captureVideoThumbnailAtTime:(Float64)seconds;

@end



@protocol MYVideoRecorderDelegate <NSObject>
@optional

// session
- (void)recorderSessionWillStart:(MYVideoRecorder *)recorder;
- (void)recorderSessionDidStart:(MYVideoRecorder *)recorder;
- (void)recorderSessionDidStop:(MYVideoRecorder *)recorder;
- (void)recorderSessionWasInterrupted:(MYVideoRecorder *)recorder;
- (void)recorderSessionInterruptionEnded:(MYVideoRecorder *)recorder;
// device / format
- (void)recorderCameraDeviceWillChange:(MYVideoRecorder *)recorder;
- (void)recorderCameraDeviceDidChange:(MYVideoRecorder *)recorder;
- (void)recorderOutputFormatWillChange:(MYVideoRecorder *)recorder;
- (void)recorderOutputFormatDidChange:(MYVideoRecorder *)recorder;
- (void)recorder:(MYVideoRecorder *)recorder didChangeCleanAperture:(CGRect)cleanAperture;
- (void)recorderDidChangeVideoFormatAndFrameRate:(MYVideoRecorder *)recorder;
// focus / exposure
- (void)recorderWillStartFocus:(MYVideoRecorder *)recorder;
- (void)recorderDidStopFocus:(MYVideoRecorder *)recorder;
- (void)recorderWillChangeExposure:(MYVideoRecorder *)recorder;
- (void)recorderDidChangeExposure:(MYVideoRecorder *)recorder;
- (void)recorderDidChangeTorchMode:(MYVideoRecorder *)recorder; // torch was changed
- (void)recorderDidChangeTorchAvailablility:(MYVideoRecorder *)recorder; // torch is available
// preview
- (void)recorderSessionDidStartPreview:(MYVideoRecorder *)recorder;
- (void)recorderSessionDidStopPreview:(MYVideoRecorder *)recorder;
// photo
- (void)recorderWillCapturePhoto:(MYVideoRecorder *)recorder;
- (void)recorderDidCapturePhoto:(MYVideoRecorder *)recorder;
- (void)recorder:(MYVideoRecorder *)recorder capturedPhoto:(nullable NSDictionary *)photoDict error:(nullable NSError *)error;
// video
- (CVPixelBufferRef)recorderWillRenderAndWritePixelBuffer:(CVPixelBufferRef)orginPixelBuffer;
- (NSString *)recorder:(MYVideoRecorder *)recorder willStartVideoCaptureToFile:(NSString *)fileName;
- (void)recorderDidStartVideoCapture:(MYVideoRecorder *)recorder;
- (void)recorderDidPauseVideoCapture:(MYVideoRecorder *)recorder; // stopped but not ended
- (void)recorderDidResumeVideoCapture:(MYVideoRecorder *)recorder;
- (void)recorderDidEndVideoCapture:(MYVideoRecorder *)recorder;
- (void)recorder:(MYVideoRecorder *)recorder capturedVideo:(nullable NSDictionary *)videoDict error:(nullable NSError *)error;
// video capture progress
- (void)recorder:(MYVideoRecorder *)recorder didCaptureVideoSampleBuffer:(CMSampleBufferRef)sampleBuffer;
- (void)recorder:(MYVideoRecorder *)recorder didCaptureAudioSample:(CMSampleBufferRef)sampleBuffer;

@end

NS_ASSUME_NONNULL_END

