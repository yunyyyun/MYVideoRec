//
//  MYVideoRecorderUtilities.h
//  MYVideoRecorder
//
//  Created by 孟云 on 20/9/29.
//  Copyright © 2020年 孟云. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface MYVideoRecorderUtilities : NSObject

+ (AVCaptureDevice *)captureDeviceForPosition:(AVCaptureDevicePosition)position;
+ (uint64_t)availableDiskSpaceInBytes;
+ (CMSampleBufferRef)createOffsetSampleBufferWithSampleBuffer:(CMSampleBufferRef)sampleBuffer withTimeOffset:(CMTime)timeOffset;
+ (NSString*)GetVideoLengthStringFromSeconds:(Float64)iVideoSeconds;
@end

@interface NSString (XMExtras)

+ (NSString *)XMformattedTimestampStringFromDate:(NSDate *)date;

@end
